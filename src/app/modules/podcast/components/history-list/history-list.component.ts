import { Component, Input, OnInit } from '@angular/core';
import { AudioService } from '@core/services/audio.service';
import { ItunesEpisode } from '@shared/models/itunes-episode.model';

@Component({
  selector: 'app-history-list',
  templateUrl: './history-list.component.html',
  styleUrls: ['./history-list.component.scss']
})
export class HistoryListComponent implements OnInit {
  @Input() public episodes: ItunesEpisode[];

  constructor(private readonly audioService: AudioService) { }

  public ngOnInit() {
  }

  public trackEpisode(index, episode) {
    return episode ? episode.src : undefined;
  }

  public setEpisode(episode) {
    this.audioService.setAudio(episode);
  }

  public stopPropagation(event: Event) {
    event.stopPropagation();
  }

}
