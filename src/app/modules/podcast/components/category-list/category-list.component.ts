import { ChangeDetectionStrategy, Component, Input, OnInit } from '@angular/core';
import { ItunesCategory } from '@shared/models/itunes-category.models';

@Component({
  selector: 'app-category-list',
  templateUrl: './category-list.component.html',
  styleUrls: ['./category-list.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})

export class CategoryListComponent implements OnInit {

  @Input() public categories: ItunesCategory[];
  private readonly colors = {
    1301: '#339989',
    1303: '#465c69',
    1304: '#3f2a2b',
    1305: '#2a2b2a',
    1307: '#5e4955',
    1309: '#586c5d',
    1310: '#505856',
    1311: '#3d4850',
    1314: '#575d2a',
    1315: '#3f3a37',
    1316: '#575c4b',
    1318: '#a8763e',
    1321: '#6f1a07',
    1323: '#57302b',
    1324: '#576660',
    1325: '#4c2c45'
  };

  constructor() {
  }

  public ngOnInit() {
  }

  public trackCategory(index, category) {
    return category ? category.id : undefined;
  }

  public getCategoryIcon(id) {
    return `assets/img/categories/${id}.png`;
  }

  public getColor(id) {
    return this.colors[id];
  }
}
