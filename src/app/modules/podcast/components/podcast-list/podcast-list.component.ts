import { Component, Input, OnChanges, OnInit } from '@angular/core';
import { animate, query, stagger, state, style, transition, trigger } from '@angular/animations';
import { ItunesPodcast } from '@shared/models/itunes-podcast.models';

@Component({
  selector: 'app-podcast-list',
  templateUrl: './podcast-list.component.html',
  styleUrls: ['./podcast-list.component.scss'],
})
export class PodcastListComponent implements OnInit {

  @Input() public podcasts: ItunesPodcast[];
  @Input() public topPodcastLoaded = false;

  constructor() {
  }

  public ngOnInit() {
  }

  public trackPodcast(index, podcast) {
    return podcast ? podcast.id : undefined;
  }
}
