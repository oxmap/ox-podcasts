import { Component, Input, OnInit } from '@angular/core';
import { ItunesPodcast } from '@shared/models/itunes-podcast.models';

@Component({
  selector: 'app-feed-header',
  templateUrl: './feed-header.component.html',
  styleUrls: ['./feed-header.component.scss']
})
export class FeedHeaderComponent implements OnInit {

  @Input()
  public podcast: ItunesPodcast;

  constructor() { }

  public ngOnInit() {
  }
}
