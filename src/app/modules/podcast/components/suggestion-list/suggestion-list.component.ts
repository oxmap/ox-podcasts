import { Component, Input, OnChanges, OnInit } from '@angular/core';
import { animate, query, stagger, state, style, transition, trigger } from '@angular/animations';
import { ItunesSuggestion } from '@shared/models/itunes-suggestions.model';

@Component({
  selector: 'app-suggestion-list',
  templateUrl: './suggestion-list.component.html',
  styleUrls: ['./suggestion-list.component.scss'],
})
export class SuggestionListComponent implements OnInit {

  @Input() public suggestions: ItunesSuggestion[];

  constructor() {
  }

  public ngOnInit() {
  }

  public trackSuggestion(index, suggestion) {
    return suggestion ? suggestion.id : undefined;
  }
}
