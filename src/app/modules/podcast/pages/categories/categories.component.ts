import { Component, OnInit } from '@angular/core';
import { AudioService } from '@core/services/audio.service';
import { ItunesCategory } from '@shared/models/itunes-category.models';
import { PodcastService } from '@core/http/podcast.service';
import { NgProgress } from 'ngx-progressbar';

@Component({
  selector: 'app-categories',
  templateUrl: './categories.component.html',
  styleUrls: ['./categories.component.scss']
})
export class CategoriesComponent implements OnInit {

  public categories: ItunesCategory[];

  constructor(
    private readonly ngProgress: NgProgress,
    private readonly podcastService: PodcastService,
    private readonly audioService: AudioService) {
  }

  public async ngOnInit() {
    try {
      this.ngProgress.start();
      this.categories = await this.podcastService.getItunesCategories();
    } catch (e) {
      console.error('Fail to load podcasts', e);
    } finally {
      this.ngProgress.done();
    }
  }
}
