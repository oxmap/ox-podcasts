import { AfterViewInit, Component, ElementRef, OnInit, ViewChild } from '@angular/core';
import { BehaviorSubject  } from 'rxjs/BehaviorSubject';
import { ItunesPodcast } from '@shared/models/itunes-podcast.models';
import { ItunesSuggestion } from '@shared/models/itunes-suggestions.model';
import { PodcastService } from '@core/http/podcast.service';
import { NgProgress } from 'ngx-progressbar';

@Component({
  selector: 'app-search',
  templateUrl: './search.component.html',
  styleUrls: ['./search.component.scss']
})

export class SearchComponent implements OnInit, AfterViewInit {
  public searchResult: ItunesPodcast[];
  public searchSuggestions: ItunesSuggestion[];
  public term$ = new BehaviorSubject <string>('');

  @ViewChild('inputSearch') public inputEl: ElementRef;

  constructor(private readonly podcastService: PodcastService,
    private readonly ngProgress: NgProgress) {
    this.podcastService
      .searchItunesPodcastOnKeyUp(this.term$)
      .subscribe((response) => {
        this.searchSuggestions = response.map((podcast) => {
          const titleId = podcast.title.toLowerCase().indexOf(this.term$.getValue());
          const authorId = podcast.author.toLowerCase().indexOf(this.term$.getValue());
          let termText = podcast.author;
          if (titleId < 0) {
            termText = podcast.author;
          } else if (authorId < 0) {
            termText = podcast.title;
          } else if (titleId < authorId) {
            termText = podcast.title;
          }

          return { id: podcast.id, text: termText};
        });
        this.ngProgress.done();
      });
  }

  public keyUp(event: KeyboardEvent) {
    this.ngProgress.start();
    const enterKey = 13;
    if (event.keyCode === enterKey) {
      this.showAllResults();
    }

    const target = <HTMLInputElement>event.target;
    this.term$.next(target.value);
  }

  public ngAfterViewInit() {
    window.scrollTo(0, 0);
    this.inputEl.nativeElement.focus();
  }

  public ngOnInit() {
  }

  public showAllResults() {
    this.searchSuggestions = [];
    this.podcastService
      .searchItunesPodcast(this.term$.getValue())
      .subscribe((response) => {
        this.searchResult = response;
        this.ngProgress.done();
      });
  }

  public notFound() {
    return (this.searchResult !== undefined) && (this.searchResult.length === 0);
  }
}
