import { AfterViewInit, Component, OnInit } from '@angular/core';
import { AudioService } from '@core/services/audio.service';
import { PouchdbAudioHistoryService } from '@core/services/pouchdb-audio-history.service';
import { ItunesEpisode } from '@shared/models/itunes-episode.model';
import { NgProgress } from 'ngx-progressbar';

@Component({
  selector: 'app-history',
  templateUrl: './history.component.html',
  styleUrls: ['./history.component.scss']
})
export class HistoryComponent implements OnInit, AfterViewInit {
  public episodes: ItunesEpisode[];

  constructor(
    private readonly ngProgress: NgProgress,
    private readonly pouchdbAudioHistoryService: PouchdbAudioHistoryService,
    private readonly audioService: AudioService) { }

  public async ngOnInit() {
    try {
      this.ngProgress.start();
      const time = 100;
      const { docs: episodes } = await this.pouchdbAudioHistoryService
        .query({ lastPlay: { '$gte': null } }, { lastPlay: 'desc' }, time);

      this.episodes = episodes;
      this.ngProgress.done();
    } catch (e) {
      console.error('Fail to get last epsodes', e);
      this.ngProgress.done();
    }
  }

  public ngAfterViewInit() {
    window.scrollTo(0, 0);
  }
}
