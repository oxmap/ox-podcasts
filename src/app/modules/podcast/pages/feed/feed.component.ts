import { AfterViewInit, Component, ElementRef, Input, OnChanges, OnInit, ViewChild } from '@angular/core';
import { ActivatedRoute, Params } from '@angular/router';
import { AudioService } from '@core/services/audio.service';
import { PouchdbSubscribeService } from '@core/services/pouchdb-subscribe.service';
import { ItunesPodcast } from '@shared/models/itunes-podcast.models';
import { PodcastService } from '@core/http/podcast.service';
import { NgProgress } from 'ngx-progressbar';

@Component({
  selector: 'app-feed',
  templateUrl: './feed.component.html',
  styleUrls: ['./feed.component.scss']
})
export class FeedComponent implements OnInit, AfterViewInit {

  public podcast: ItunesPodcast;

  public isSubscribed = true;
  public isRefreshing = false;

  @ViewChild('podcastInfo') public podcastInfo: ElementRef;

  constructor(private readonly podcastService: PodcastService,
    private readonly ngProgress: NgProgress,
    private readonly router: ActivatedRoute,
    private readonly audioService: AudioService,
    private readonly pouchdbSubscribeService: PouchdbSubscribeService) { }

  public ngOnInit() {
    this.ngProgress.start();
    this.router.params.subscribe(async (params: Params) => {
      await this.loadFeedFromDb(params.id);

      if (!this.podcast) {
        this.loadFeedFromWeb(params.id);
      }
      if (!this.podcast) {
        this.isSubscribed = false;
      }
    });
  }

  public ngAfterViewInit() {
    window.scrollTo(0, 0);
  }

  public async loadFeedFromDb(feedId: string) {
    try {
      this.podcast = await this.pouchdbSubscribeService.getOne(feedId);
      this.ngProgress.done();
    } catch (e) {
    } finally {
    }
  }

  public async loadFeedFromWeb(feedId: string) {
    this.isRefreshing = true;
    try {
      const podcast = await this.podcastService.getPodcastById(feedId);
      this.podcast = await this.podcastService.getFeed(podcast);
    } catch (e) {
      console.error('Fail to load Feed', e);
    } finally {
      this.ngProgress.done();
      this.isRefreshing = false;
    }
  }

  public setEpisode(episode) {
    this.audioService.setAudio(episode);
  }

  public playFeed() {
    this.audioService.setPlaylist(this.podcast.episodes);
  }

  public async subscribeFeed(podcast: ItunesPodcast) {
    try {
      await this.pouchdbSubscribeService.putOne(String(podcast.id), {...podcast, _rev: undefined, subscribed: true});
      this.isSubscribed = true;
    } catch (e) {
      console.error('Failt to subscribe', e);
    }
  }

  public async unsubscribeFeed(podcast: ItunesPodcast) {
    try {
      await this.pouchdbSubscribeService.removeOne(String(podcast.id));
      this.isSubscribed = false;
    } catch (e) {
      console.error('Fail to unsubscribe', e);
    }
  }

  public async updateFeed() {
    await this.loadFeedFromWeb(this.podcast.id);
    this.pouchdbSubscribeService.putOne(String(this.podcast.id), this.podcast);
  }

  public stopPropagation(event: Event) {
    event.stopPropagation();
  }

}
