import { AfterViewInit, Component, OnInit } from '@angular/core';
import { ActivatedRoute, Params } from '@angular/router';
import { ItunesPodcast } from '@shared/models/itunes-podcast.models';
import { PodcastService } from '@core/http/podcast.service';
import { NgProgress } from 'ngx-progressbar';

@Component({
  selector: 'app-category',
  templateUrl: './category.component.html',
  styleUrls: ['./category.component.scss']
})
export class CategoryComponent implements OnInit, AfterViewInit {

  public podcasts: ItunesPodcast[];
  public categoryName: string;

  constructor(private readonly podcastService: PodcastService,
    private readonly ngProgress: NgProgress,
    private readonly router: ActivatedRoute) {
    const count = 40;
    this.podcasts = new Array(count).fill(new ItunesPodcast());
  }

  public ngOnInit() {
    this.ngProgress.start();
    this.router.params.subscribe(async (params: Params) => {
      try {
        this.categoryName = params.name;
        this.podcasts = await this.podcastService.searchPodcastByCategory(params.id);
      } catch (e) {
        console.error('fail to load category', e);
      } finally {
        this.ngProgress.done();
      }
    });
  }

  public ngAfterViewInit() {
    window.scrollTo(0, 0);
  }

}
