import { AfterViewInit, Component, OnInit } from '@angular/core';
import { ItunesCategory } from '@shared/models/itunes-category.models';
import { ItunesPodcast } from '@shared/models/itunes-podcast.models';
import { PodcastService } from '@core/http/podcast.service';
import { NgProgress } from 'ngx-progressbar';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss']
})
export class HomeComponent implements OnInit, AfterViewInit {

  public categories: ItunesCategory[];
  public podcasts: ItunesPodcast[];

  constructor(
    private readonly podcastService: PodcastService,
    private readonly ngProgress: NgProgress
  ) {
    const listCount = 20;
    this.podcasts = new Array(listCount).fill(new ItunesPodcast());
  }

  public async ngOnInit() {
    try {
      this.ngProgress.start();

      this.categories = await this.podcastService.getItunesCategories();
      this.podcasts = await this.podcastService.getItunesTopPodcast();
    } catch (e) {
      console.error('Fail to load podcasts', e);
    } finally {
      this.ngProgress.done();
    }
  }

  public ngAfterViewInit() {
    window.scrollTo(0, 0);
  }
}
