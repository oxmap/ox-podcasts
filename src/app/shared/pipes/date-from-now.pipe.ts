import { Pipe, PipeTransform } from '@angular/core';
import * as moment from 'moment';

@Pipe({
  name: 'dateFromNow'
})
export class DateFromNowPipe implements PipeTransform {

  constructor() {
    moment.locale('ru');
  }

  public transform(value: any, args?: any): any {
    return moment(value).fromNow();
  }

}
