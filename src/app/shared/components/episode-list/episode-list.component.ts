import { Component, Input, OnInit } from '@angular/core';
import { AudioService } from '@core/services/audio.service';
import { ItunesEpisode } from '@shared/models/itunes-episode.model';
import * as moment from 'moment';

@Component({
  selector: 'app-episode-list',
  templateUrl: './episode-list.component.html',
  styleUrls: ['./episode-list.component.scss']
})
export class EpisodeListComponent implements OnInit {

  @Input() public episodes: ItunesEpisode[];
  @Input() public bPLaylist = false;
  public curId: string;

  constructor(private readonly audioService: AudioService) { }

  public ngOnInit() {
    this.audioService.changeAudioEvent
      .subscribe((episode: ItunesEpisode) => {
        this.curId = episode.guid;
    });
  }

  public getTitle(episode) {
    return episode.title;
  }

  public setEpisode(episode) {
    this.audioService.setAudio(episode, this.bPLaylist);
  }

  public stopPropagation(event: Event) {
    event.stopPropagation();
  }

  public trackEpisode(index, episode) {
    return episode ? episode.src : undefined;
  }

  public convertDateTime(utcDate) {
    moment.locale('ru');

    return moment(utcDate).format('LL');
  }

  public convertDuration(duration) {
    const second = 2;
    const third = 3;

    return (duration.substr(0, second) === '00') ? duration.substr(third) : duration;
  }
}
