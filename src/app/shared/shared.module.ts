import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { DateFromNowPipe } from '@shared/pipes/date-from-now.pipe';
import { EpisodeListComponent } from '@shared/components/episode-list/episode-list.component';
import { ToHttpsPipe } from './pipes/to-https.pipe';

@NgModule({
  imports: [
    CommonModule
  ],
  declarations: [
    DateFromNowPipe,
    EpisodeListComponent,
    ToHttpsPipe
  ],
  providers: [
    DateFromNowPipe,
    ToHttpsPipe
  ],
  exports: [
    DateFromNowPipe,
    EpisodeListComponent,
    ToHttpsPipe
  ]
})
export class SharedModule {
  public static forRoot() {
    return {
        ngModule: SharedModule,
        providers: [],
    };
 }
}
