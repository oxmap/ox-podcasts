import { ItunesEpisode } from '@shared/models/itunes-episode.model';

export class ItunesPodcast {
    public id = '';
    public author = '';
    public cover = '/assets/img/top-podcast-placeholder.png';
    public title = '';
    public description: string;
    public feedUrl: string;
    public trackCount: number;
    public primaryGenreName: string;
    public lastUpdate: Date;
    public episodes: ItunesEpisode[];
}
