export class PouchDoc {
  public offset: 0;
  public rows: [null];
  public totalRows: 0;
}
