export class ItunesEpisode {
  public author: string;
  public cover: string;
  public currentTime = 0;
  public currentTimeString = '00:00';
  public description: string;
  public duration = 0;
  public durationString = '00:00';
  public guid: string;
  public lastPlay: Date;
  public podcastId: string;
  public podcastTitle: string;
  public progressbar = 0;
  public releaseDate: Date;
  public size: string;
  public sizeBytes = 0;
  public src: string;
  public title: string;
  public type: string;
}
