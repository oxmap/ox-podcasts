import { Component, Input, OnDestroy, OnInit } from '@angular/core';
import { AudioService } from '@core/services/audio.service';
import { PouchdbAudioPlaylistService } from '@core/services/pouchdb-audio-playlist.service';
import { ItunesEpisode } from '@shared/models/itunes-episode.model';

@Component({
  selector: 'app-listen-query',
  templateUrl: './listen-query.component.html',
  styleUrls: ['./listen-query.component.scss']
})
export class ListenQueryComponent implements OnInit, OnDestroy {

  @Input() public longlist: ItunesEpisode[];
  public showView: boolean;

  constructor(
    private readonly audioService: AudioService,
    private readonly pouchdbAudioPlaylistService: PouchdbAudioPlaylistService) {
    this.showView = true;
  }

  public ngOnInit() {
    const body = document.getElementsByTagName('body')[0];
    body.style.overflow = 'hidden';

    this.getAudios()
    .then((res) => {
      this.longlist = res;
      this.audioService.longlist = res;
    })
    .catch((err) => {
      console.error(err);
    });
  }

  public ngOnDestroy() {
    const body = document.getElementsByTagName('body')[0];
    body.style.overflow = 'visible';
  }

  public getAudios(): Promise<any> {
    try {
      return this.pouchdbAudioPlaylistService.getAll().then((playlist) => {
        return playlist.rows
        .map((row) => row.doc)
        .filter((doc) => 'author' in doc)
        .sort((a, b) => new Date(b.releaseDate).getTime() - new Date(a.releaseDate).getTime());
      });
    } catch (e) {
      console.error('Fail to get query list', e);
    }
  }

  public stopPropagation(event: Event) {
    event.stopPropagation();
  }

  public trackEpisode(index, episode) {
    return episode ? episode.src : undefined;
  }

  public clear() {
    this.pouchdbAudioPlaylistService.clearDb()
    .then((res) => {
      this.longlist = [];
    })
    .catch((err) => {
      console.error(err);
    });
  }

  public closeView() {
    this.showView = false;
  }
}
