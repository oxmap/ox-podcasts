import { Component, HostListener, OnDestroy, OnInit } from '@angular/core';
import * as moment from 'moment';
import { AudioService } from '../../../core/services/audio.service';
import { PouchdbAudioPlaylistService } from '../../services/pouchdb-audio-playlist.service';
import { ItunesEpisode } from '@shared/models/itunes-episode.model';

@Component({
  selector: 'app-audio-player',
  templateUrl: './audio-player.component.html',
  styleUrls: ['./audio-player.component.scss']
})
export class AudioPlayerComponent implements OnInit, OnDestroy {
  public episode: ItunesEpisode = new ItunesEpisode();
  public audio: HTMLAudioElement = new Audio();
  public showListenQuery: boolean;

  constructor(
    private readonly audioService: AudioService,
    private readonly pouchdbPlaylistbAudioService: PouchdbAudioPlaylistService
  ) {
    this.showListenQuery = false;
  }

  public async ngOnInit() {
    this.episode = await this.getLastAudio();
    this.loadAudio(false);

    this.audioService.changeAudioEvent
      .subscribe((episode: ItunesEpisode) => {
        this.episode.currentTime = 0;
        this.episode = episode;
        this.loadAudio();
      });
  }

  public ngOnDestroy() {
    if (this.audio) {
      this.audio.pause();
      this.audio = null;
    }
  }

  @HostListener('document:keypress', ['$event'])
  public handleKeyPress(event: KeyboardEvent) {
    const target = event.target as HTMLElement;
    const spaceKey = 32;
    if (event.keyCode === spaceKey && target.tagName !== 'INPUT') {
      event.preventDefault();
      this.toggleAudio();
    }
  }

  @HostListener('document:keydown', ['$event'])
  public handleKeyDown(event: KeyboardEvent) {
    const target = event.target as HTMLElement;
    const rightArrow = 39;
    const leftArrow = 37;
    const sec = 5;
    if (event.keyCode === leftArrow && target.tagName !== 'INPUT') {
      event.preventDefault();
      this.audio.currentTime -= sec;
    } else if (event.keyCode === rightArrow && target.tagName !== 'INPUT') {
      event.preventDefault();
      this.audio.currentTime += sec;
    }
  }

  public loadAudio(autoPlay: boolean = true): void {
    if (!this.audio) {
      this.audio = new Audio();
    }
    if (!this.episode) {
      this.episode = new ItunesEpisode();
    }

    this.audio.pause();
    this.audio.src = this.episode.src;
    this.audio.currentTime = this.episode.currentTime || 0;
    this.audio.load();

    if (autoPlay) {
      this.audio.play();
    }

    this.audio.onloadedmetadata = () => {
      this.episode.durationString = this.getTimeString(this.audio.duration);
      this.episode.currentTimeString = this.getTimeString(this.audio.currentTime);
    };

    this.audio.ontimeupdate = async () => {
      try {
        this.episode.currentTimeString = this.getTimeString(this.audio.currentTime);
        this.episode.currentTime = this.audio.currentTime;

        const progressbarValue = this.audio.currentTime / this.audio.duration;
        this.episode.progressbar = isNaN(progressbarValue) ? 0 : progressbarValue;
      } catch (e) {
        console.error('Fail to update track info', e);
      }
    };

    this.audio.onended = () => {
      this.audioService.nextEpisode();
    };
  }

  public async getLastAudio(): Promise<ItunesEpisode> {
    try {
      const queryRes = await this.pouchdbPlaylistbAudioService.query(
        {$and: [
          { name: {'$gt': null} },
          { name: {'$exists': true} }
        ]},
        // { lastPlay: { '$gte': null } },
        { lastPlay: 'desc' }, 1);

      return <ItunesEpisode>queryRes.docs[0];

    } catch (e) {
      console.error('Fail to get last audio', e);

      return new ItunesEpisode();
    }
  }

  public seekAudio(event: MouseEvent): void {
    const percent = event.offsetX / (event.target as HTMLElement).offsetWidth;
    this.audio.currentTime = percent * this.audio.duration;
  }

  public prev() {
    this.audioService.prevEpisode();
  }

  public next() {
    this.audioService.nextEpisode();
  }

  public toggleAudio(): void {
    if (this.audio.paused) {
      this.audio.play();
    } else {
      this.audio.pause();
    }
  }

  public getTimeString(timeInSecs: number): string {
    const timeString = moment('2015-01-01')
      .startOf('day')
      .seconds(timeInSecs)
      .format('H:mm:ss');

    return timeString;
  }

  public setVolume(e): void {
    this.audio.volume = parseFloat(e.value);
  }

  public getVolumeIcon(val): string {
    const maybeZero = (val === 0.0) ? 'volume_off' : 'volume_down';
    const half = 0.5;

    return (val > half) ? 'volume_up' : maybeZero;
  }

  public toggleSpeed(e): void {
    /* tslint:disable */
    const speeds = [0.25, 0.5, 0.75, 1.0, 1.25, 1.5, 2.0];
    const idx = speeds.findIndex((k) => (k === this.audio.playbackRate)) + 1;
    this.audio.playbackRate = idx === speeds.length ? 0.25 : speeds[idx];
  }

  public getSpeed(): string {
    return `x${this.audio.playbackRate}`;
  }

}
