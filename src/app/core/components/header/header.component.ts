import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router, UrlSegment } from '@angular/router';
import { Location } from '@angular/common';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.scss']
})
export class HeaderComponent implements OnInit {

  public homeUrls: string[] = ['/', '/home', '/search', '/subscribes', '/history', '/categories'];
  public isHome = true;
  public headerTitle: string;

  constructor(private readonly router: Router,
    private readonly location: Location
  ) {}

  public ngOnInit() {
    this.router.events.subscribe((val) => {
      this.isHome = [
        ...this.homeUrls,
        ...this.homeUrls.map((v) => `/podcast${v}`)
      ].indexOf(this.router.url) > 0;
    });
  }
}
