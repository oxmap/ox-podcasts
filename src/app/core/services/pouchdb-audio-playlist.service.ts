import { Injectable } from '@angular/core';
import PouchDB from 'pouchdb';
import PouchDBFind from 'pouchdb-find';
import { PouchDoc } from '@shared/models/pouch.model';

PouchDB.plugin(PouchDBFind);

@Injectable()
export class PouchdbAudioPlaylistService {
  public db: any;

  constructor() {
    if (!this.db) {
      this.db = new PouchDB('ox-podcasts-audio-playlist');
    }
    this.createIndexes();
  }

  public getAll(): Promise<any> {
    try {
      return this.db.allDocs({include_docs: true});
    } catch (err) {
      console.error(err);
    }
  }

  public async clearDb(): Promise<any> {
    this.db.allDocs({include_docs: true}).then((allDocs) => {
      return allDocs.rows.map((row) => {
        return {_id: row.id, _rev: row.doc._rev, _deleted: true};
      });
    }).then((deleteDocs) => {
      return this.db.bulkDocs(deleteDocs);
    });
  }

  public query(query: any, sort: any, limit: number = 1): Promise<any> {
    return this.db.find({
      selector: query,
      sort: [sort],
      limit
    });
  }

  public getOne(id: string): Promise<any> {
    return this.db.get(id);
  }

  public async putOne(id: string, document: any): Promise<any> {
    let doc = null;

    try {
      doc = await this.getOne(id);
    } catch (e) {
      const notFound = 404;
      if (e.status !== notFound) { throw new Error(e); }
    }
    if (doc) {
      return this.db.put({ ...document, _id: id, _rev: doc._rev, lastPlay: new Date() });
    }

    await this.clearDb();

    return this.db.put({ ...document, _id: id, lastPlay: new Date() });
  }

  public async removeOne(id: string): Promise<any> {
    let doc = null;
    console.log(id);
    try {
      doc = await this.getOne(id);
    } catch (e) {
      throw new Error(e);
    }

    return this.db.remove(doc);
  }

  public async putList(ids: string[], documents: any): Promise<any> {
    await this.clearDb();

    return this.db.bulkDocs([...documents]);
  }

  private async createIndexes() {
    try {
      await this.db.createIndex({ index: { fields: ['lastPlay'] } });
    } catch (err) {
      console.error(err);
    }
  }
}
