import { Injectable } from '@angular/core';
import PouchDB from 'pouchdb';
import PouchDBFind from 'pouchdb-find';

PouchDB.plugin(PouchDBFind);

@Injectable()
export class PouchdbAudioHistoryService {
  public db: any;

  constructor() {
    if (!this.db) {
      this.db = new PouchDB('ox-podcasts-audio-history');
    }
    this.createIndexes();
  }

  public getAll(): Promise<any> {
    try {
      return this.db.allDocs({include_docs: true});
    } catch (err) {
      console.error(err);
    }
  }

  public query(query: any, sort: any, limit: number = 1): Promise<any> {
    return this.db.find({
      selector: query,
      sort: [sort],
      limit
    });
  }

  public getOne(id: string): Promise<any> {
    return this.db.get(id);
  }

  public async putOne(id: string, document: any): Promise<any> {
    let doc = null;

    try {
      doc = await this.getOne(id);
    } catch (e) {
      const notFound = 404;
      if (e.status !== notFound) { throw new Error(e); }
    }
    if (doc) {
      return this.db.put({ ...document, _id: id, _rev: doc._rev, lastPlay: new Date() });
    }

    return this.db.put({ ...document, _id: id, lastPlay: new Date() });
  }

  public async putList(ids: string[], documents: any): Promise<any> {
    return this.db.bulkDocs([ ...documents]);
  }

  private async createIndexes() {
    try {
      await this.db.createIndex({ index: { fields: ['lastPlay'] } });
    } catch (err) {
      console.error(err);
    }
  }
}
