import { EventEmitter, Injectable } from '@angular/core';
import { PouchdbAudioPlaylistService } from './pouchdb-audio-playlist.service';
import { PouchdbAudioHistoryService } from './pouchdb-audio-history.service';
import { ItunesEpisode } from '@shared/models/itunes-episode.model';

@Injectable()
export class AudioService {
  public changeAudioEvent: EventEmitter<ItunesEpisode> = new EventEmitter<ItunesEpisode>();
  public longlist: ItunesEpisode[];
  private curId: string;

  constructor(
    private readonly pouchdbAudioPlaylistService: PouchdbAudioPlaylistService,
    private readonly pouchdbAudioHistoryService: PouchdbAudioHistoryService,
    ) { }

  public async setAudio(episode: ItunesEpisode, fromPlaylist: boolean = false) {
    try {
      this.curId = episode.src;
      if (!fromPlaylist) {
        await this.pouchdbAudioPlaylistService.putOne(episode.src, episode);
      }

      const cachedEpisode = await this.getCachedAudio(episode.src);
      if (cachedEpisode) {
        return this.changeAudioEvent.emit(cachedEpisode);
      }

      if (!fromPlaylist) {
        await this.pouchdbAudioHistoryService.putOne(episode.src, episode);
      }
      this.changeAudioEvent.emit(episode);
    } catch (e) {
        console.error('Fail to set current episode', e);
    }
  }

  public async setPlaylist(episodes: ItunesEpisode[]) {
    try {
      this.curId = episodes[0].src;
      await this.pouchdbAudioPlaylistService.putList(episodes.map((ep) => ep.src), episodes);
      this.changeAudioEvent.emit(episodes[0]);
      } catch (e) {
      console.error('Fail to set current playlist', e);
    }
  }

  public nextEpisode() {
    this.stepEpisode(1);
  }

  public prevEpisode() {
    this.stepEpisode(-1);
  }

  private async getCachedAudio(id: string) {
    try {
      return <ItunesEpisode>await this.pouchdbAudioHistoryService.getOne(id);
    } catch (e) {
      const notFound = 404;
      if (e.status === notFound) {
        return;
      }
      console.error('Fail to get cached audio', e);

      return;
    }
  }

  private stepEpisode(step: number) {
    const [episode] = this.longlist.filter((ep) => ep.src === this.curId);
    const nextEpisode = this.longlist[this.longlist.indexOf(episode) + step];
    this.curId = nextEpisode.src;
    this.changeAudioEvent.emit(nextEpisode);
  }

}
