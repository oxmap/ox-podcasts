import { BrowserModule } from '@angular/platform-browser';
import { APP_INITIALIZER, NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { HTTP_INTERCEPTORS, HttpClientModule } from '@angular/common/http';

import { PouchdbSubscribeService } from './core/services/pouchdb-subscribe.service';
import { AppComponent } from './app.component';
import { AppRoutingModule } from './app-routing.module';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { MatSliderModule } from '@angular/material/slider';
import { AudioService } from './core/services/audio.service';
import { PouchdbAudioPlaylistService } from './core/services/pouchdb-audio-playlist.service';
import { PouchdbAudioHistoryService } from './core/services/pouchdb-audio-history.service';
import { PodcastModule } from './modules/podcast/podcast.module';
import { HeaderComponent } from './core/components/header/header.component';
import { ListenQueryComponent } from './core/components/listen-query/listen-query.component';
import { AudioPlayerComponent } from './core/components/audio-player/audio-player.component';
import { SharedModule } from '@shared/shared.module';
import { PodcastService } from '@core/http/podcast.service';
import { environment } from 'environments/environment';
import { ServiceWorkerModule } from '@angular/service-worker';
import { NgProgressInterceptor, NgProgressModule } from 'ngx-progressbar';

export const podcastProvider = (provider: PodcastService) => {
  return () => provider.getLocation();
};

@NgModule({
  declarations: [
    AppComponent,
    AudioPlayerComponent,
    HeaderComponent,
    ListenQueryComponent
  ],
  imports: [
    ServiceWorkerModule.register('/ngsw-worker.js',  { enabled: environment.production }),
    BrowserModule,
    BrowserAnimationsModule,
    HttpClientModule,
    NgProgressModule,
    PodcastModule,
    AppRoutingModule,
    SharedModule.forRoot(),
    MatSliderModule
  ],
  providers: [
    PouchdbAudioPlaylistService,
    PouchdbAudioHistoryService,
    PouchdbSubscribeService,
    AudioService,
    { provide: APP_INITIALIZER, useFactory: podcastProvider, deps: [PodcastService], multi: true }
  ],
  bootstrap: [AppComponent]
})

export class AppModule { }
